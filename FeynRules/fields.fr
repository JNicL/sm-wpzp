(*============================================================================*)
(*                             Field definitions                              *)
(*============================================================================*)

M$ClassesDescription = {

(* Gauge bosons: physical vector fields *)
  V[1] == {
    ClassName       -> A,
    SelfConjugate   -> True,
    Mass            -> 0,
    Width           -> 0,
    ParticleName    -> "A",
    PDG             -> 22,
    TeXParticleName -> "\\gamma",
    PropagatorLabel -> "A",
    PropagatorType  -> W,
    PropagatorArrow -> None,
    FullName        -> "Photon"
  },
  V[2] == {
    ClassName       -> Z,
    SelfConjugate   -> True,
    Mass            -> {MZ, 91.1876},
    Width           -> {WZ, 2.4952},
    ParticleName    -> "Z",
    PDG             -> 23,
    PropagatorLabel -> "Z",
    PropagatorType  -> Sine,
    PropagatorArrow -> None,
    FullName        -> "Z"
  },
  V[6] == {
    ClassName       -> Zp,
    SelfConjugate   -> True,
    Mass            -> {MZp, 2100.},
    Width           -> {WZp, 1.},
    ParticleName    -> "Zp",
    PropagatorLabel -> "Zp",
    PropagatorType  -> Sine,
    PropagatorArrow -> None,
    TeXParticleName -> "Z'"
  },
  V[3] == {
    ClassName           -> W,
    SelfConjugate       -> False,
    Mass                -> {MW, 80.399},
    Width               -> {WW, 2.085},
    ParticleName        -> "W+",
    AntiParticleName    -> "W-",
    QuantumNumbers      -> {Q -> 1},
    PDG                 -> 24,
    TeXParticleName     -> "W^+",
    TeXAntiParticleName -> "W^-",
    PropagatorLabel     -> "W",
    PropagatorType      -> Sine,
    PropagatorArrow     -> Forward,
    FullName            -> "W"
  },
  (* W' boson *)
  V[5] == {
	  ClassName           -> Wp,
    SelfConjugate       -> False,
	  Mass                -> {MWp, 2000.},
	  Width               -> {WWp, 1.},
	  ParticleName        -> "Wp+",
	  AntiParticleName    -> "Wp-",
    QuantumNumbers      -> {Q->1},
    TeXParticleName     -> "W'^+",
    TeXAntiParticleName -> "W'^-",
	  PropagatorLabel     -> "Wp",
	  PropagatorType      -> Sine,
	  PropagatorArrow     -> Forward
  },
  V[4] == {
    ClassName        -> G,
    SelfConjugate    -> True,
    Indices          -> {Index[Gluon]},
    Mass             -> 0,
    Width            -> 0,
    ParticleName     -> "g",
    PDG              -> 21,
    PropagatorLabel  -> "G",
    PropagatorType   -> C,
    PropagatorArrow  -> None,
    FullName         -> "G"
  },

(* Ghosts: related to physical gauge bosons *)
  U[1] == {
    ClassName           -> ghA,
    SelfConjugate       -> False,
    Ghost               -> A,
    QuantumNumbers      -> {GhostNumber -> 1},
    Mass                -> 0,
    PropagatorLabel     -> "uA",
    TeXParticleName     -> "u_{\\gamma}",
    TeXAntiParticleName -> "\\bar{u}_{\\gamma}",
    PropagatorType      -> GhostDash,
    PropagatorArrow     -> Forward
  },
  U[2] == {
    ClassName           -> ghZ,
    SelfConjugate       -> False,
    Ghost               -> Z,
    QuantumNumbers      -> {GhostNumber -> 1},
    Mass                -> {MZ,91.1876},
    PropagatorLabel     -> "uZ",
    TeXParticleName     -> "u_Z",
    TeXAntiParticleName -> "\\bar{u}_Z",
    PropagatorType      -> GhostDash,
    PropagatorArrow     -> Forward
  },
  U[31] == {
    ClassName           -> ghWp,
    SelfConjugate       -> False,
    Ghost               -> W,
    QuantumNumbers      -> {GhostNumber -> 1, Q -> 1},
    Mass                -> {MW, 80.399},
    PropagatorLabel     -> "uWp",
    TeXParticleName     -> "u_{W^+}",
    TeXAntiParticleName -> "\\bar{u}_{W^+}",
    PropagatorType      -> GhostDash,
    PropagatorArrow     -> Forward
  },
  U[32] == {
    ClassName           -> ghWm,
    SelfConjugate       -> False,
    Ghost               -> Wbar,
    QuantumNumbers      -> {GhostNumber -> 1, Q -> -1},
    Mass                -> {MW, 80.399},
    PropagatorLabel     -> "uWm",
    TeXParticleName     -> "u_{W^-}",
    TeXAntiParticleName -> "\\bar{u}_{W^-}",
    PropagatorType      -> GhostDash,
    PropagatorArrow     -> Forward
  },
  U[4] == {
    ClassName           -> ghG,
    SelfConjugate       -> False,
    Indices             -> {Index[Gluon]},
    Ghost               -> G,
    PDG                 -> 82,
    QuantumNumbers      ->{GhostNumber -> 1},
    Mass                -> 0,
    PropagatorLabel     -> "uG",
    TeXParticleName     -> "u_{G-}",
    TeXAntiParticleName -> "\\bar{u}_{G}",
    PropagatorType      -> GhostDash,
    PropagatorArrow     -> Forward
  },

(* Gauge bosons: unphysical vector fields *)
  V[11] == {
    ClassName     -> B,
    Unphysical    -> True,
    SelfConjugate -> True,
    Definitions   -> {B[mu_] -> (sw*Z[mu] + cw*A[mu])}
  },
  V[12] == {
    ClassName     -> Wi,
    Unphysical    -> True,
    SelfConjugate -> True,
    Indices       -> {Index[SU2W]},
    FlavorIndex   -> SU2W,
    Definitions   -> {Wi[mu_,1] -> (Wbar[mu]+W[mu])/Sqrt[2],
                      Wi[mu_,2] -> (Wbar[mu]-W[mu])/(I*Sqrt[2]),
                      Wi[mu_,3] -> (cw*Z[mu] - sw*A[mu])}
  },

(* Ghosts: related to unphysical gauge bosons *)
  U[11] == {
    ClassName     -> ghB,
    Unphysical    -> True,
    SelfConjugate -> False,
    Ghost         -> B,
    Definitions   -> {ghB -> sw ghZ + cw ghA}
  },
  U[12] == {
    ClassName     -> ghWi,
    Unphysical    -> True,
    SelfConjugate -> False,
    Ghost         -> Wi,
    Indices       -> {Index[SU2W]},
    FlavorIndex   -> SU2W,
    Definitions   -> {ghWi[1] -> (ghWp + ghWm)/Sqrt[2],
                      ghWi[2] -> (ghWm - ghWp)/(I*Sqrt[2]),
                      ghWi[3] -> cw ghZ - sw ghA}
  } ,

(* Fermions: physical fields *)
  F[1] == {
    ClassName           -> vl,
    ClassMembers        -> {ve,vm,vt},
    Indices             -> {Index[Generation]},
    FlavorIndex         -> Generation,
    SelfConjugate       -> False,
    Mass                -> 0,
    Width               -> 0,
    QuantumNumbers      -> {LeptonNumber -> 1},
    PropagatorLabel     -> {"v", "nu_e", "nu_mu", "nu_tau"} ,
    PropagatorType      -> S,
    PropagatorArrow     -> Forward,
    PDG                 -> {12,14,16},
    ParticleName        -> {"nu_e", "nu_mu", "nu_tau"},
    AntiParticleName    -> {"nu_e~","nu_mu~","nu_tau~"},
    TeXParticleName     -> {"\\nu_e", "\\nu_\\mu", "\\nu_\\tau"},
    TeXAntiParticleName -> {"\\bar{\\nu}_e", "\\bar{\\nu}_\\mu", "\\bar{\\nu}_\\tau"},
    FullName            -> {"Electron-neutrino", "Mu-neutrino", "Tau-neutrino"}
  },
  F[2] == {
    ClassName           -> l,
    ClassMembers        -> {e, mu, ta},
    Indices             -> {Index[Generation]},
    FlavorIndex         -> Generation,
    SelfConjugate       -> False,
    Mass                -> {Ml, {ME,5.11*^-4}, {MM,0.10566}, {MTA,1.777}},
    Width               -> {0, {WM, 0.01}, {WTA, 0.01}},
    QuantumNumbers      -> {Q -> -1, LeptonNumber -> 1},
    PropagatorLabel     -> {"l", "e", "mu", "tau"},
    PropagatorType      -> Straight,
    PropagatorArrow     -> Forward,
    PDG                 -> {11, 13, 15},
    ParticleName        -> {"e-", "mu-", "tau-"},
    AntiParticleName    -> {"e+", "mu+", "tau+"},
    TeXParticleName     -> {"e^-", "\\mu^-", "\\tau^-"},
    TeXAntiParticleName -> {"e^+", "\\mu^+", "\\tau^+"},
    FullName            -> {"Electron", "Muon", "Tau"}
  },
  F[3] == {
    ClassName           -> uq,
    ClassMembers        -> {u, c, t},
    Indices             -> {Index[Generation], Index[Colour]},
    FlavorIndex         -> Generation,
    SelfConjugate       -> False,
    Mass                -> {Mu, {MU, 2.55*^-3}, {MC,1.27}, {MT,172}},
    Width               -> {0, {WC, 0.01}, {WT,1.50833649}},
    QuantumNumbers      -> {Q -> 2/3},
    PropagatorLabel     -> {"uq", "u", "c", "t"},
    PropagatorType      -> Straight,
    PropagatorArrow     -> Forward,
    PDG                 -> {2, 4, 6},
    ParticleName        -> {"u",  "c",  "t" },
    AntiParticleName    -> {"u~", "c~", "t~"},
    TeXParticleName     -> {"u", "c", "t"},
    TeXAntiParticleName -> {"\\bar{u}", "\\bar{c}", "\\bar{t}"},
    FullName            -> {"u-quark", "c-quark", "t-quark"}
  },
  F[4] == {
    ClassName           -> dq,
    ClassMembers        -> {d, s, b},
    Indices             -> {Index[Generation], Index[Colour]},
    FlavorIndex         -> Generation,
    SelfConjugate       -> False,
    Mass                -> {Md, {MD,5.04*^-3}, {MS,0.101}, {MB,4.7}},
    Width               -> {0, 0, {WB, 0.01}},
    QuantumNumbers      -> {Q -> -1/3},
    PropagatorLabel     -> {"dq", "d", "s", "b"},
    PropagatorType      -> Straight,
    PropagatorArrow     -> Forward,
    PDG                 -> {1,3,5},
    ParticleName        -> {"d",  "s",  "b" },
    AntiParticleName    -> {"d~", "s~", "b~"},
    TeXParticleName     -> {"d", "s", "b"},
    TeXAntiParticleName -> {"\\bar{d}", "\\bar{s}", "\\bar{b}"},
    FullName            -> {"d-quark", "s-quark", "b-quark"}
  },

(* Fermions: unphysical fields *)
(*Hypercharges adjusted to Ansgars convention*)
  F[11] == {
    ClassName      -> LL,
    Unphysical     -> True,
    Indices        -> {Index[SU2D], Index[Generation]},
    FlavorIndex    -> SU2D,
    SelfConjugate  -> False,
    QuantumNumbers -> {Y -> -1},
    Definitions    -> {LL[sp1_,1,ff_] :> Module[{sp2},ProjM[sp1,sp2]vl[sp2,ff]],
                       LL[sp1_,2,ff_] :> Module[{sp2},ProjM[sp1,sp2] l[sp2,ff]]}
  },
  F[12] == {
    ClassName      -> lR,
    Unphysical     -> True,
    Indices        -> {Index[Generation]},
    FlavorIndex    -> Generation,
    SelfConjugate  -> False,
    QuantumNumbers -> {Y -> -2},
    Definitions    -> {lR[sp1_,ff_] :> Module[{sp2}, ProjP[sp1,sp2] l[sp2,ff]]}
  },
  F[13] == {
    ClassName      -> QL,
    Unphysical     -> True,
    Indices        -> {Index[SU2D], Index[Generation], Index[Colour]},
    FlavorIndex    -> SU2D,
    SelfConjugate  -> False,
    QuantumNumbers -> {Y -> 1/3},
    Definitions    -> {
      QL[sp1_,1,ff_,cc_] :> Module[{sp2}, ProjM[sp1,sp2] uq[sp2,ff,cc]],
      QL[sp1_,2,ff_,cc_] :> Module[{sp2,ff2}, CKM[ff,ff2] ProjM[sp1,sp2] dq[sp2,ff2,cc]] }
  },
  F[14] == {
    ClassName      -> uR,
    Unphysical     -> True,
    Indices        -> {Index[Generation], Index[Colour]},
    FlavorIndex    -> Generation,
    SelfConjugate  -> False,
    QuantumNumbers -> {Y -> 4/3},
    Definitions    -> {uR[sp1_,ff_,cc_] :> Module[{sp2},
                       ProjP[sp1,sp2] uq[sp2,ff,cc]] }
  },
  F[15] == {
    ClassName      -> dR,
    Unphysical     -> True,
    Indices        -> {Index[Generation], Index[Colour]},
    FlavorIndex    -> Generation,
    SelfConjugate  -> False,
    QuantumNumbers -> {Y -> -2/3},
    Definitions    -> {dR[sp1_,ff_,cc_] :> Module[{sp2},
                       ProjP[sp1,sp2] dq[sp2,ff,cc]] }
  },
(* Higgs: physical scalars  *)
  S[1] == {
    ClassName       -> H,
    SelfConjugate   -> True,
    Mass            -> {MH,120},
    Width           -> {WH,0.00575308848},
    PropagatorLabel -> "H",
    PropagatorType  -> D,
    PropagatorArrow -> None,
    PDG             -> 25,
    ParticleName    -> "H",
    FullName        -> "H"
  },

(* Higgs: physical scalars  *)
  S[2] == {
    ClassName       -> G0,
    SelfConjugate   -> True,
    Goldstone       -> Z,
    Mass            -> {MZ, 91.1876},
    Width           -> WGo,
    PropagatorLabel -> "Go",
    PropagatorType  -> D,
    PropagatorArrow -> None,
    PDG             -> 250,
    ParticleName    -> "G0",
    TeXParticleName -> "G_0",
    FullName        -> "G0"
  },
  S[3] == {
    ClassName           -> GP,
    SelfConjugate       -> False,
    Goldstone           -> W,
    Mass                -> {MW, 80.370881514409731},
    QuantumNumbers      -> {Q -> 1},
    Width               -> WGP,
    PropagatorLabel     -> "GP",
    PropagatorType      -> D,
    PropagatorArrow     -> None,
    PDG                 -> 251,
    ParticleName        -> "G+",
    AntiParticleName    -> "G-",
    TeXParticleName     -> "G^+",
    TeXAntiParticleName -> "G^{-}",
    FullName            -> "GP"
  },
  S[12] == {
    ClassName       -> Gp0,
    SelfConjugate   -> True,
    Goldstone       -> Zp,
    Mass            -> {MZp, 2100.},
    Width           -> {WZp, 1.},
    PropagatorLabel -> "Gp0",
    PropagatorType  -> D,
    PropagatorArrow -> None,
    ParticleName    -> "Gp0",
    TeXParticleName -> "G'_0",
    FullName        -> "Gp0"
  },
  S[13] == {
    ClassName           -> GpP,
    SelfConjugate       -> False,
    Goldstone           -> Wp,
    Mass                -> {MWp, 2000.},
    QuantumNumbers      -> {Q -> 1},
	  Width               -> {WWp, 1.},
    PropagatorLabel     -> "GpP",
    PropagatorType      -> D,
    PropagatorArrow     -> None,
    ParticleName        -> "Gp+",
    AntiParticleName    -> "Gp-",
    TeXParticleName     -> "G'^+",
    TeXAntiParticleName -> "G'^{-}",
    FullName            -> "GpP"
  },
(* Higgs: unphysical scalars  *)
  S[11] == {
    ClassName      -> Phi,
    Unphysical     -> True,
    Indices        -> {Index[SU2D]},
    FlavorIndex    -> SU2D,
    SelfConjugate  -> False,
    QuantumNumbers -> {Y -> 1},
    Definitions    -> {Phi[1] -> GP, Phi[2] -> (vev + H + I G0)/Sqrt[2]}
  }
 };
